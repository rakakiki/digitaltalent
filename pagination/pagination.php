<?php
    include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pagination</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <center>
            <h2>Pagination PHP, MySQL dan Bootstrap</h2>
        </center> <br>

        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Nomor</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Alamat</th>
                 </tr>
            </thead>
            <tbody>
                <?php
                    $batas = 10;
                    $halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
                    $halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;

                    $previous = $halaman - 1;
                    $next = $halaman + 1;

                    $data = mysqli_query($koneksi, "SELECT * FROM pegawai");
                    $jumlah_data = mysqli_num_rows($data);
                    $total_halaman = ceil($jumlah_data / $batas);

                    $data_pegawai = mysqli_query($koneksi, "SELECT * FROM pegawai limit $total_halaman, $batas");
                    $nomor = $halaman_awal +1;
                    while($d = mysqli_fetch_array($data_pegawai)) {
                        ?>
                        <tr>
                            <td><?= $nomor++; ?></td>
                            <td><?= $d['pegawai_nama']; ?></td>
                            <td><?= $d['pegawai_umur']; ?></td>
                            <td><?= $d['pegawai_alamat']; ?></td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>

        <nav>
			<ul class="pagination justify-content-center">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ echo "href='?halaman=$previous'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_halaman;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="?halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?halaman=$next'"; } ?>>Next</a>
				</li>
			</ul>
		</nav>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>