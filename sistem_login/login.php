<?php
    $server = "localhost";
    $user = "root";
    $password = '';
    $db = "tutorial";

    $koneksi = mysqli_connect($server, $user, $password, $db);
    if (!$koneksi) {
        die("<script>alert('Gagal tersambung dengan database.')</script>");
    }
    error_reporting(0);

    session_start();
    if (isset($_SESSION['username'])) {
        header("Location: berhasil_login.php");
    }
    if (isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
        $res = mysqli_query($koneksi, $sql);
        if($res->num_rows > 0) {
            $row = mysqli_fetch_assoc($res);
            $_SESSION['username'] = $row['username'];
            header("Location: berhasil_login.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form action="" method="POST">
            <p style="font-size: 2rem; font-weight: 800;">Login</p>
            <input type="text" placeholder="Username" name="username" value="<?php echo $username; ?>" required><br>
            <input type="password" placeholder="Password" name="password" value="<?php echo $_POST['password']; ?>" required><br>
            <button name="submit" class="btn btn-success">Login</button><br>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>